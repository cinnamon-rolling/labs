package student.onlineretailer;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		
		CartService service = context.getBean(CartServiceImpl.class);

		service.addItemToCart(0, 1);
		service.addItemToCart(1, 3);
		service.addItemToCart(2, 5);

		service.removeItemFromCart(1);
		
		double totalCost = service.calculateCartCost();
		System.out.printf("total cost is %.2f\n", totalCost);

	}

	@Bean
	public Map<Integer, Item> catalog() {
		Map<Integer, Item> items = new HashMap<>();

		items.put(0, new Item(0, "desc1", 100.00));
		items.put(1, new Item(1, "desc1", 200.00));
		items.put(2, new Item(2, "desc1", 300.00));
		return items;
	}
}
