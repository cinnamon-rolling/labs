package student.onlineretailer;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CartServiceImpl implements CartService{
    @Autowired
    private CartRepository repository;

    // @Autowired
    // public CartServiceImpl(CartRepository repository){};

	@Value("#{catalog}")	
    private Map<Integer, Item> catalog;

    @Override
    public void addItemToCart(int id, int quantity){
        if (catalog.containsKey(id)) {
            repository.add(id, quantity);
        }
    }

    @Override
    public void removeItemFromCart(int id) {
        repository.remove(id);
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return repository.getAll();
    }

    public double calculateCartCost() {
        double totalCost = 0;

        Map<Integer, Integer> items = repository.getAll();
        for (Map.Entry<Integer, Integer> item: items.entrySet()) {
            double cost = catalog.get(item.getKey()).getPrice() * item.getValue();
            totalCost += cost;    
        }
        return totalCost;
    }
}
