const axios = require("axios");

const URL = "https://localhost:8080";

const fetchDate = () => {
    axios
        .get(URL + "/date")
        .then((response) => {
            const dateData = response.data.data;
            console.log(`date`, dateData);
        })
        .catch((error) => console.error(error));
};

fetchDate();

const fetchTime = () => {
    axios
        .get(URL + "/time")
        .then((response) => {
            const timeData = response.data.data;
            console.log(`time`, timeData);
        })
        .catch((error) => console.error(error));
};

fetchTime();
