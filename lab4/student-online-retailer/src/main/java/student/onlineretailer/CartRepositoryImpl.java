package student.onlineretailer;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class CartRepositoryImpl implements CartRepository {
    
    private Map<Integer, Integer> cart = new HashMap<>();

    @Override
    public void add(int itemId, int quantity) {
        Integer currentQuantity = cart.get(itemId);
        if (currentQuantity != null) {
            quantity += currentQuantity;
        }
        cart.put(itemId, quantity);
    }

	@Override
	public void remove(int itemId) {
		cart.remove(itemId);
	}
	
	@Override
	public Map<Integer, Integer> getAll() {
		return cart;
	} 
}