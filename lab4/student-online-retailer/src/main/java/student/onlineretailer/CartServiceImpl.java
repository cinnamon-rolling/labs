package student.onlineretailer;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CartServiceImpl implements CartService{
    @Autowired
    private CartRepositoryImpl repository;

	@Value("#{catalog}")	
    private Map<Integer, Item> catalog;

	@Value("${contactEmail}")
    private String contactEmail;

    @Value("${onlineRetailer.salesTaxRate}")
	private double salesTaxRate;
    
	@Value("${onlineRetailer.deliveryCharge.normal}")
	private double standardDeliveryCharge;
	
	@Value("${onlineRetailer.deliveryCharge.threshold}")
	private double deliveryChargeThreshold;
    
    public String getContactEmail() {
        return contactEmail;
    }

    public double getSalesTaxRate() {
        return salesTaxRate;
    }

    @Override
    public void addItemToCart(int id, int quantity){
        if (catalog.containsKey(id)) {
            repository.add(id, quantity);
        }
    }
    
    @Override
    public void removeItemFromCart(int id) {
        repository.remove(id);
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return repository.getAll();
    }

    public double calculateCartCost() {
        double totalCost = 0;

        Map<Integer, Integer> items = repository.getAll();
        for (Map.Entry<Integer, Integer> item: items.entrySet()) {
            double cost = catalog.get(item.getKey()).getPrice() * item.getValue();
            totalCost += cost;    
        }
        return totalCost;
    }
}
