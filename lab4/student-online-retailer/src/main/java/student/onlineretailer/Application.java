package student.onlineretailer;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		
		CartServiceImpl service = context.getBean(CartServiceImpl.class);

		// contact email
		System.out.printf("Contact email is: %s\n", service.
		getContactEmail());
		
		System.out.printf("Sales tax rate: %.2f\n", service.getSalesTaxRate());

		service.addItemToCart(0, 1);
		service.addItemToCart(1, 3);
		service.addItemToCart(2, 5);

		service.removeItemFromCart(1);
		
		double totalCost = service.calculateCartCost();
		System.out.printf("total cost is %.2f\n", totalCost);

		// Get profile-specific properties.
        ResourcesBean resourcesBean = context.getBean(ResourcesBean.class);
        System.out.println("Profile-specific properties: " + resourcesBean);
	}

	@Bean
	public Map<Integer, Item> catalog() {
		Map<Integer, Item> items = new HashMap<>();

		items.put(0, new Item(0, "desc1", 100.00));
		items.put(1, new Item(1, "desc2", 200.00));
		items.put(2, new Item(2, "desc3", 300.00));
		return items;
	}
}
