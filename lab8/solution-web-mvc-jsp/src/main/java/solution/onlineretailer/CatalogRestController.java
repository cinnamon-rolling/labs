package solution.onlineretailer;

import java.net.URI;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/catalog")
@CrossOrigin
public class CatalogRestController {
    
    @Autowired
    private Map<Integer, Item> catalog;

    @GetMapping(produces = {"application/json", "application/xml"})
    public ResponseEntity<Collection<Item>> getAllItemsInCatalog() {
        return ResponseEntity.ok().body(catalog.values());
    }

    @GetMapping(value = "/{id}", produces = {"application/json", "application/xml"})
    public ResponseEntity<Item> getItemInCatalog(@PathVariable int id) {
        if (catalog.containsKey(id)) {
            return ResponseEntity.ok().body(catalog.get(id));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public ResponseEntity<Item> addItemToCatalog(@RequestBody Item item){
        catalog.put(item.getId(), item);
        URI uri = URI.create("/catalog/" + item.getId());
        return ResponseEntity.created(uri).body(item);
    }

    @PutMapping(value="/{id}", consumes={"application/json","application/xml"})
    public ResponseEntity modifyItemInCatalog(@PathVariable int id, @RequestBody Item item) {
        if (!catalog.containsKey(id))
            return ResponseEntity.notFound().build();
        else {
            catalog.put(id, item);
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteItemInCatalog(@PathVariable int id) {
        if (!catalog.containsKey(id))
            return ResponseEntity.notFound().build();
        else {
            catalog.remove(id);
            return ResponseEntity.ok().build();
        }
    }

}
